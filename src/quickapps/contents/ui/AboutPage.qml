// SPDX-FileCopyrightText: 2023 Laurent Montel <montel@kde.org>
// SPDX-License-Identifier: LGPL-2.0-or-later

import QtQuick 2.1
import org.kde.kirigami 2.19 as Kirigami
import org.kde.kdebugsettings 1.0
import org.kde.kirigamiaddons.labs.mobileform 0.1 as MobileForm

MobileForm.AboutPage {
    aboutData: About
}
